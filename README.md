# first project

## basic configuration
 
```
git config --global user.name "first_project"
git config --global user.email "t.benmessabih@esi-sba.dz"
```

## existing folder

```
cd existing folder
git init
git remote add origin remote_repository
git add .
git commit -m 'first commit'
git push -u origin master
```
